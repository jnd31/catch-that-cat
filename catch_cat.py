import pygame, sys
from pygame.locals import *
import random

def isInBox(x, y, objx, objy, xcoord, ycoord, width, height):
    if (x > (xcoord - objx) and x < (xcoord + width) and y > (ycoord - objy) and y < (ycoord + height)):
        return True
    else:
        return False

def main():
    pygame.init()

    FPS = 30 #frames per second setting
    fpsClock = pygame.time.Clock()

    #set up the window
    dim = [500,300] # dimensions of the window: [x,y]
    hitbox = [100, 75] # dimensions of the cat hitbox [x, y]
    maxSpeed = 100 # max magnitude of the vector the cat goes on. Modded by calmness
    boxx = 150 # x dimension of boxes
    boxy = 100 # y dimension of boxes
    offset = 10 # offset cat start and boxes from the edge
    '''
    To agitate the cat, it holds over a juice box for a certain number
    of frames. calmCountBuffer is the number of frames it holds over
    calming juice, agiCountBuffer is the number of frames it holds
    over agitating juice. agiCount and calmCount count how many frames
    it has been over the juice, and once they hit the limit, they 
    increase or decrease the calmess.
    '''
    agiCount = 0
    calmCount = 0
    calmCountbuffer = 20
    agiCountbuffer = 20
    '''
    The cat moves in vectors, and vectorBuffer is calmness basically.
    vectorBuffer notes how many frames it will take for the cat to reach
    the end of the vector. The calmer the cat is, the more frames it takes.
    '''
    vectorBuffer = 10
    vectorCountdown = vectorBuffer # counts how many frames to the end
    calmness = 10 # initial calmness
    maxCalm = 50
    minCalm = 1 # DON'T GO BELOW ZERO
    agiboxx = dim[0] - boxx - offset # the x edge of agitating box
    bothboxy = dim[1] - boxy - offset # the y edge of both boxes
    RED = (255,0,0)
    BLUE = (0,0,255)
    WHITE = (255,255,255)
    DISPLAYSURF = pygame.display.set_mode((dim[0],dim[1]),0,32)
    pygame.display.set_caption('Catch that cat!')
    catImg = pygame.image.load('cat.png')
    cat = [dim[0] // 2, offset]
    #direction = 'right'
    increment = [0,0]
    while True:
        DISPLAYSURF.fill(WHITE)
        pygame.draw.rect(DISPLAYSURF,RED,(agiboxx,bothboxy,boxx,boxy))
        pygame.draw.rect(DISPLAYSURF,BLUE,(offset,bothboxy,boxx,boxy))
        if vectorBuffer == vectorCountdown:
            vector = [random.randint((-(maxSpeed + calmness)),(maxSpeed + calmness)), random.randint((-maxSpeed),maxSpeed)]
            if (cat[0] + vector[0]) < (-hitbox[0]//2):
                vector[0] = (-hitbox[0]//2) - cat[0]
            elif (cat[0] + vector[0]) > (dim[0] - hitbox[0]//2):
                vector[0] = (dim[0] - hitbox[0]//2) - cat[0]
            if (cat[1] + vector[1]) < (-hitbox[1]//2):
                vector[1] = (-hitbox[1]//2) - cat[1]
            elif (cat[1] + vector[1]) > (dim[1] - hitbox[1]//2):
                vector[1] = (dim[1] - hitbox[1]//2) - cat[1]
            vectorCountdown = 0
            vectorBuffer = calmness
            #print(vector)
        
        vectorCountdown += 1
        for i in range (2):
            if vector[i] > 0:
                cat[i] += vector[i]//vectorBuffer + 1
            else:
                cat[i] += vector[i]//vectorBuffer
        '''if (cat[0]) < (-1 * hitbox[0]):
            cat[0] = (-1 * hitbox[0])
        elif (cat[0]) > (dim[0] + 1):
            cat[0] = (dim[0] + 1) 
        if (cat[1]) < (-1 * hitbox[1]):
            cat[1] = (-1 * hitbox[1])
        elif (cat[1]) > (dim[1] + 1):
            cat[1] = (dim[1] + 1)'''

        if isInBox(cat[0], cat[1], hitbox[0], hitbox[1], agiboxx, bothboxy, boxx, boxy):
            if agiCount < agiCountbuffer:
                agiCount += 1
            elif calmness > minCalm:
                calmness -= 1
                agiCount = 0
        elif isInBox(cat[0], cat[1], hitbox[0], hitbox[1], offset, bothboxy, boxx, boxy):
            if calmCount < calmCountbuffer:
                calmCount += 1
            elif calmness < maxCalm:
                calmness += 1
                calmCount = 0
        DISPLAYSURF.blit(catImg, (cat[0], cat[1]))
        fpsClock.tick(FPS)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

if __name__ == "__main__":
	main()