import pygame, sys
from pygame.locals import *
import random

def isinbox(x, y, objx, objy, xcoord, ycoord, width, height):
    if (x > (xcoord - objx) and x < (xcoord + width) and y > (ycoord - objy) and y < (ycoord + height)):
        return True
    else:
        return False

def main():
    pygame.init()

    FPS = 30 #frames per second setting
    fpsClock = pygame.time.Clock()

    #set up the window
    xwidth = 500
    ywidth = 300
    objsizex = 100
    objsizey = 75
    maxspeed = 20
    boxx = 150
    boxy = 100
    offset = 10
    agicount = 0
    calmcount = 0
    calmcountbuffer = 60
    agicountbuffer = 30
    agiboxx = xwidth - boxx - offset
    bothboxy = ywidth - boxy - offset
    RED = (255,0,0)
    BLUE = (0,0,255)
    WHITE = (255,255,255)
    DISPLAYSURF = pygame.display.set_mode((xwidth,ywidth),0,32)
    pygame.display.set_caption('Catch that cat!')
    '''
    Agitating the cat: it moves in a vector toward a thing, and 
    agitating the cat makes it take fewer iterations toward that thing
    and also increase the number of pixels it moves per loop
    '''

    catImg = pygame.image.load('cat.png')
    catx = (xwidth / 2)
    caty = 10
    #direction = 'right'

    while True:
        DISPLAYSURF.fill(WHITE)
        pygame.draw.rect(DISPLAYSURF,RED,(agiboxx,bothboxy,boxx,boxy))
        pygame.draw.rect(DISPLAYSURF,BLUE,(offset,bothboxy,boxx,boxy))
        
        catx += random.randint(-maxspeed,maxspeed)
        caty += random.randint(-maxspeed,maxspeed)

        if (catx) < (-1 * objsizex):
            catx = (-1 * objsizex)
        elif (catx) > (xwidth + 1):
            catx = (xwidth + 1) 
        if (caty) < (-1 * objsizey):
            caty = (-1 * objsizey)
        elif (caty) > (ywidth + 1):
            caty = (ywidth + 1)
        

        if isinbox(catx, caty, objsizex, objsizey, agiboxx, bothboxy, boxx, boxy):
            if agicount < agicountbuffer:
                agicount += 1
            else:
                maxspeed += 1
                agicount = 0
        elif isinbox(catx, caty, objsizex, objsizey, offset, bothboxy, boxx, boxy):
            if calmcount < calmcountbuffer:
                calmcount += 1
            else:
                maxspeed -= 1
                calmcount = 0
        if maxspeed < 1:
            maxspeed = 1
        
        DISPLAYSURF.blit(catImg, (catx, caty))

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        fpsClock.tick(FPS)
        pygame.display.update()

if __name__ == "__main__":
	main()